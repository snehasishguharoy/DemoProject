/**
 * 
 */
package com.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Employee;

// TODO: Auto-generated Javadoc
/**
 * The Class WelcomeController.
 *
 * @author sony
 */

@RestController

public class WelcomeController {

	static List<Employee> employees = new ArrayList<>();
	/** The employees. */
	static {

		Employee e1 = new Employee("101", "Samik", "Roy", "2000");
		Employee e2 = new Employee("102", "Rahul", "Saha", "5000");
		Employee e3 = new Employee("103", "Tanmoy", "Pal", "7000");
		employees.add(e1);
		employees.add(e2);
		employees.add(e3);

	}

	/**
	 * Gets the employees.
	 *
	 * @return the employees
	 */
	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public @ResponseBody List<Employee> getEmployees() {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		return employees;

	}

	/**
	 * Gets the employee by id.
	 *
	 * @param id
	 *            the id
	 * @return the employee by id
	 */
	@RequestMapping(value = "/getemployee/{id}", method = RequestMethod.GET)
	public @ResponseBody Employee getEmployeeById(@PathVariable("id") int id) {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		return employees.get(id);

	}

	/**
	 * Delete employee.
	 *
	 * @param id
	 *            the id
	 * @return the employee
	 */
	@RequestMapping(value = "/deleteemployee/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Employee deleteEmployee(@PathVariable("id") int id) {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		return employees.remove(id);

	}

	/**
	 * Delete employee.
	 *
	 * @param id
	 *            the id
	 * @param employee
	 *            the employee
	 * @return the employee
	 */
	@RequestMapping(value = "/updateemployee/{id}", method = RequestMethod.PUT,consumes="application/json")
	public @ResponseBody Employee updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		return employees.set(id, employee);

	}

	/**
	 * Insert employee.
	 *
	 * @param employee
	 *            the employee
	 */
	@RequestMapping(value = "/insertemployee", method = RequestMethod.POST,consumes="application/json")
	
	public @ResponseBody void insertEmployee(@RequestBody Employee employee) {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		employees.add(employee);

	}

}
