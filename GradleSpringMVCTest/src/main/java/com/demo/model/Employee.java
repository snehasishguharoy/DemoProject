package com.demo.model;

import java.io.Serializable;

public class Employee implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4414075287725374117L;
	/**
	 * 
	 */

	/**
	 * @author sony
	 *
	 */

		public String empId;
		public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

		@Override
	public String toString() {
		return "Employee [empId=" + empId + ", firstName=" + firstName + ", lastName=" + lastName + ", salary=" + salary
				+ "]";
	}

		public String firstName;
		public Employee(String empId, String firstName, String lastName, String salary) {
			super();
			this.empId = empId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.salary = salary;
		}

		public String lastName;
		public String salary;

		public String getEmpId() {
			return empId;
		}

		public void setEmpId(String empId) {
			this.empId = empId;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getSalary() {
			return salary;
		}

		public void setSalary(String salary) {
			this.salary = salary;
		}

	}


